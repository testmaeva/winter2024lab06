import java.util.Random;
public class Deck{
	private Card[] stack;
	private int numOfCards;
	private Random rng;

	//constructor
	public Deck(){
		rng = new Random();
		this.numOfCards = 52;
		this.stack = new Card[numOfCards];

		String[] type = {"Spades", "Diamonds", "Clubs", "Hearts"};
		int[] number = {1,2,3,4,5,6,7,8,9,10,11,12,13};
		int i=0;
		for(int t=0; t<type.length; t++){
			for(int n=0; n<number.length; n++){
				stack[i] = new Card(type[t], number[n]);
				i++;
			}
		}
	}
	
	// setter/getter
	public void setNumOfCards (int numOfCards){
		this.numOfCards = numOfCards;
	}

	//num of cards
	public int length(){
		return this.numOfCards;
	}

	//top card
	public String drawTopCard(){
		return stack[stack.length-1].toString();
	}

	//to string
	public String toString(){
		final String RED = "\u001B[31m";
		final String BLACK = "\u001B[30m";
		final String BACKGROUND = "\u001B[47m";
		final String RESET = "\033[0m";

		String cards = "";
		for(int i=0; i<this.numOfCards; i++){
			if(stack[i].getSuit().equals("Spades")){
				cards+= BACKGROUND+BLACK+ stack[i].toString() + "♠" +RESET+"\n";
			}
			if(stack[i].getSuit().equals("Diamonds")){
				cards+= BACKGROUND+RED+ stack[i].toString() + "♦" +RESET+"\n";
			}
			if(stack[i].getSuit().equals("Clubs")){
				cards+= BACKGROUND+BLACK+ stack[i].toString() + "♣" +RESET+"\n";
			}
			if(stack[i].getSuit().equals("Hearts")){
				cards+= BACKGROUND+RED+ stack[i].toString() + "♥" +RESET+"\n";
			}
		}
		return cards;
	}

	//shuffle
	public void shuffle(){
		for(int i=0; i<numOfCards; i++){
			int position = rng.nextInt(numOfCards);
			Card placeholder = stack[i];
			stack[i] = stack[position];
			stack[position] = placeholder;
		}
	}
}