import java.util.Random;
import java.util.Scanner;
public class LuckyCardGameApp{
    public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		Deck deck = new Deck();
		deck.shuffle();
		System.out.println("Hello hello!! How many cards do u wish to take off?");
		int cardsToRemove = reader.nextInt();
		reader.nextLine();
		
		String validation = "";
		
		if (cardsToRemove>deck.length()){
			System.out.println("The deck is not that long fam, try again");
			cardsToRemove = reader.nextInt();
			reader.nextLine();
		}
		else{
			System.out.println("R u sure?" + "\n" + "(Yes/No)");
			validation = reader.nextLine();
		}
		
		while(cardsToRemove>deck.length() || validation.toLowerCase().equals("no")){
			if (cardsToRemove>deck.length()){
				System.out.println("Deck's still not that long");
			}
			else{
				System.out.println("How many cards do u wish to take off then?");
			}
			
			cardsToRemove = reader.nextInt();
			reader.nextLine();
			
			if (cardsToRemove<=deck.length()){
				System.out.println("R u sure?" + "\n" + "(Yes/No)");
				validation = reader.nextLine();
			}
		}
		System.out.println("U had " +deck.length()+ " cards in ur deck" +"\n"+ "u took out " +cardsToRemove+ " cards" );
		
		deck.setNumOfCards(deck.length() - cardsToRemove);
		
		System.out.println("This is how many cards you have left in ur deck: " +deck.length()+ "\n");
		deck.shuffle();
		System.out.println(deck);
		
    }
}